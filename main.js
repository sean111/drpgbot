const blessed = require("blessed");
const Bot = require("./Bot");
const chalk = require("chalk");
const bot = new Bot();

const screen = blessed.screen({
    smartCSR: true
});

screen.title = "dRPG Bot";

const logDisplay = blessed.box({
    top: 0,
    left: 0,
    height: '100%-1',
    width: '100%',
    keys: true,
    mouse: true,
    alwaysScroll: true,
    scrollable: true,
    scrollbar: {
        ch: ' ',
        bg: 'red'
    }
});

const inputBar = blessed.textbox({
    bottom: 0,
    left: 0,
    height: 1,
    width: '100%',
    keys: true,
    mouse: true,
    autoFocus: true,
    inputOnFocus: true,
    style: {
        fg: 'white',
        bg: 'blue'	// Blue background so you see this is different from body
    }
});

bot.event.addListener('bot.log', (message) => {
    logDisplay.pushLine(message);
    screen.render();
});

screen.append(logDisplay);
screen.append(inputBar);

screen.render();
// Close the example on Escape, Q, or Ctrl+C
screen.key(['C-c'], (ch, key) => (process.exit(0)));

inputBar.key("enter", (ch, key) => {
    let text = inputBar.getValue();
    let args = text.split(" ");
    let command = args[0];
    args.shift();
    bot.event.emit("bot.command", command, args);
    logDisplay.pushLine(chalk`{green Command:} ${command} {yellow Args:} ${args.join("|")}`);
    inputBar.clearValue();
    inputBar.focus();
    screen.render();
});

screen.key("enter", (ch, key) => {
   inputBar.focus();
});

bot.start();