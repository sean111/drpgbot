const log = console.log;
const jsonFile = require("jsonfile");
const eris = require("eris");
const EventEmitter = require("events");
const chalk = require("chalk");
const _ = require("lodash");

let configFile = __dirname + "/config.json";

class Bot {
    constructor() {
        this.config = {};
        this.client = null;
        this.event = new EventEmitter();
        this.event.on("bot.command", (command, args) => {
            this.event.emit("bot.log", `Received => Command: ${command} - Args: ${args.join(",")}`);
            switch (command) {
                case "exit": {
                    process.exit(0);
                    break;
                }
                case "config": {
                    if (args[0] === "set") {
                        let key = args[1];
                        let newValue = args[2];
                        if (_.has(this.config, key)) {
                            this.event.emit("bot.log", `New value for ${key} is ${newValue}`);
                            this.config[key] = newValue;
                            jsonFile.writeFile(configFile, this.config, (err) => {
                                if (err) {
                                    this.event.emit("bot.log", chalk`{white.bgRed ${err}`);
                                }
                            });
                        }
                    } else {
                        let configString = chalk`{yellow.underline Available Commands}\n`;
                        _.forOwn(this.config, (value, key) => {
                            configString += chalk`{green ${key}}: ${value}\n`;
                        });
                        this.event.emit("bot.log", configString);
                    }
                    break;
                }
            }
        });
    }
    start() {
        jsonFile.readFile(configFile, (err, obj) => {
            if (err) {
                log.error(err);
            } else {
                this.config = obj;
                this.client = new eris(obj.token);
                this.client.connect().catch(err => console.error(err));
                this.client.on("ready", () => {
                    this.event.emit("bot.log", "Bot connected...")
                });
                this.client.on("messageCreate", msg => {
                    if (msg.channel.id === this.config.targetChannel) {
                        log(msg.content);
                        this.event.emit("bot.log", msg.content);
                    }
                });
            }
        });
    }
}

module.exports = Bot;